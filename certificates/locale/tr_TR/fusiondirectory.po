# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:50+0000\n"
"Language-Team: Turkish (Turkey) (https://www.transifex.com/fusiondirectory/teams/12202/tr_TR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: tr_TR\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: personal/certificates/class_userCertificates.inc:31
#: personal/certificates/class_userCertificates.inc:32
msgid "Download"
msgstr ""

#: personal/certificates/class_userCertificates.inc:75
msgid "Unknown format"
msgstr ""

#: personal/certificates/class_userCertificates.inc:116
#: personal/certificates/class_userCertificates.inc:133
msgid "Certificates"
msgstr ""

#: personal/certificates/class_userCertificates.inc:117
msgid "User certificates"
msgstr ""

#: personal/certificates/class_userCertificates.inc:137
msgid "Certificate content"
msgstr ""
