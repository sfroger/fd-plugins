# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:59+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: German (https://www.transifex.com/fusiondirectory/teams/12202/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: config/personal/class_personalConfig.inc:26
msgid "Personal configuration"
msgstr "Persönliche Konfiguration"

#: config/personal/class_personalConfig.inc:27
msgid "FusionDirectory personal plugin configuration"
msgstr ""

#: config/personal/class_personalConfig.inc:40
#: personal/personal/class_personalInfo.inc:85
msgid "Personal"
msgstr "Persönlich"

#: config/personal/class_personalConfig.inc:43
msgid "Allow use of private email for password recovery"
msgstr ""

#: config/personal/class_personalConfig.inc:43
msgid "Allow users to use their private email address for password recovery"
msgstr ""

#: personal/personal/class_personalInfo.inc:31
msgid "Site"
msgstr "Seite"

#: personal/personal/class_personalInfo.inc:31
msgid "Website the account is on"
msgstr "Webseite des Kontos ist auf"

#: personal/personal/class_personalInfo.inc:37
msgid "Id"
msgstr "Kennung"

#: personal/personal/class_personalInfo.inc:37
msgid "Id of this user on this website"
msgstr "Kennung von diesem Benutzer auf dieser Webseite"

#: personal/personal/class_personalInfo.inc:86
msgid "Personal information"
msgstr "Persönliche Informationen"

#: personal/personal/class_personalInfo.inc:103
msgid "Personal info"
msgstr "Persönliche Information"

#: personal/personal/class_personalInfo.inc:106
msgid "Personal title"
msgstr "Titel"

#: personal/personal/class_personalInfo.inc:106
msgid "Personal title - Examples of personal titles are \"Ms\", \"Dr\", \"Prof\" and \"Rev\""
msgstr ""

#: personal/personal/class_personalInfo.inc:111
msgid "Nickname"
msgstr "Kurzname"

#: personal/personal/class_personalInfo.inc:111
msgid "Nicknames for this user"
msgstr "Spitznamen für diesen Benutzer"

#: personal/personal/class_personalInfo.inc:116
msgid "Badge Number"
msgstr ""

#: personal/personal/class_personalInfo.inc:116
msgid "Company badge number"
msgstr ""

#: personal/personal/class_personalInfo.inc:120
msgid "Date of birth"
msgstr "Geburtsdatum"

#: personal/personal/class_personalInfo.inc:127
msgid "Sex"
msgstr "Geschlecht"

#: personal/personal/class_personalInfo.inc:127
msgid "Gender"
msgstr "Geschlecht"

#: personal/personal/class_personalInfo.inc:132
msgid "Country"
msgstr "Land"

#: personal/personal/class_personalInfo.inc:136
msgid "Start date"
msgstr "Startdatum"

#: personal/personal/class_personalInfo.inc:136
msgid "Date this user joined the company"
msgstr ""

#: personal/personal/class_personalInfo.inc:141
msgid "End date"
msgstr "Enddatum"

#: personal/personal/class_personalInfo.inc:141
msgid "Date this user is supposed to leave the company"
msgstr ""

#: personal/personal/class_personalInfo.inc:146
msgid "Photo Visible"
msgstr "Foto sichtbar"

#: personal/personal/class_personalInfo.inc:146
msgid "Should the photo of the user be visible on external tools"
msgstr ""

#: personal/personal/class_personalInfo.inc:152
msgid "Contact"
msgstr "Kontakt"

#: personal/personal/class_personalInfo.inc:156
msgid "Social account"
msgstr ""

#: personal/personal/class_personalInfo.inc:156
msgid "Social accounts of this user"
msgstr ""

#: personal/personal/class_personalInfo.inc:163
msgid "Private email"
msgstr "Private E-Mail"

#: personal/personal/class_personalInfo.inc:163
msgid "Private email addresses of this user"
msgstr "Private E-Mail-Adresse dieses Benutzers"

#: personal/personal/class_socialHandlers.inc:69
msgid "Facebook"
msgstr "Facebook"

#: personal/personal/class_socialHandlers.inc:79
msgid "Twitter"
msgstr "Twitter"

#: personal/personal/class_socialHandlers.inc:99
msgid "Diaspora*"
msgstr "Diaspora*"

#: personal/personal/class_socialHandlers.inc:112
msgid "Diaspora accounts must look like user@pod"
msgstr ""

#: personal/personal/class_socialHandlers.inc:122
msgid "LinkedIn"
msgstr "LinkedIn"

#: personal/personal/class_socialHandlers.inc:132
msgid "ORCID"
msgstr ""

#: personal/personal/class_socialHandlers.inc:145
msgid ""
"ORCID account IDs must look like XXXX-XXXX-XXXX-XXXX where X are digits"
msgstr ""

#: personal/personal/class_socialHandlers.inc:148
msgid "Incorrect ORCID value, the checksum does not match"
msgstr ""
