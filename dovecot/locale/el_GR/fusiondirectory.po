# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2018
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2020-05-15 09:03+0000\n"
"PO-Revision-Date: 2018-08-13 19:53+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2018\n"
"Language-Team: Greek (Greece) (https://www.transifex.com/fusiondirectory/teams/12202/el_GR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: el_GR\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:26
#: admin/systems/services/dovecot/class_serviceDovecot.inc:27
msgid "Dovecot (IMAP/POP3)"
msgstr "Dovecot (IMAP/POP3)"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:27
msgid "Services"
msgstr "Υπηρεσίες"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:42
msgid "Dovecot connection"
msgstr ""

#: admin/systems/services/dovecot/class_serviceDovecot.inc:45
msgid "Connect URL for Dovecot server"
msgstr "URL σύνδεσης σε διακομιστή Dovecot"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:49
msgid "Hostname"
msgstr "Όνομα συστήματος"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:49
msgid "Hostname of the Dovecot server"
msgstr "Όνομα συστήματος του διακομιστή Dovecot"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:53
msgid "Port"
msgstr "Θύρα"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:53
msgid "Port number on which Dovecot server should be contacted"
msgstr "Αριθμός θύρας για την επικοινωνία με τον διακομιστή Dovecot"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:58
msgid "Option"
msgstr "Επιλογή"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:58
msgid "Options for contacting Dovecot server"
msgstr "Επιλογές για επικοινωνία με διακομιστή Dovecot"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:63
msgid "Validate TLS/SSL Certificates"
msgstr ""

#: admin/systems/services/dovecot/class_serviceDovecot.inc:63
msgid "Weither or not to validate server certificates on connection"
msgstr ""

#: admin/systems/services/dovecot/class_serviceDovecot.inc:75
msgid "Master credentials"
msgstr "Κύρια διαπιστευτήρια"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:78
msgid "Admin user"
msgstr "Διαχειριστής"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:78
msgid "Dovecot server admin user"
msgstr "Διαχειριστής διακομιστή Dovecot"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:82
msgid "Password"
msgstr "Κωδικός πρόσβασης:"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:82
msgid "Admin user password"
msgstr "Κωδικός διαχειριστή"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:86
msgid "Mail directory"
msgstr "Κατάλογος αλληλογραφίας:"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:86
msgid "Path to the directory in which user maildirs must be created"
msgstr ""
"Διαδρομή για τον κατάλογος στο οποίο θα πρέπει να δημιουργηθούν τα maildirs"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:93
msgid "Options"
msgstr " Επιλογές"

#: admin/systems/services/dovecot/class_serviceDovecot.inc:96
msgid "Create the user folder via Argonaut"
msgstr ""

#: admin/systems/services/dovecot/class_serviceDovecot.inc:96
msgid "Use argonaut to create the user mailbox on the Dovecot server"
msgstr ""
"Χρήση argonaut για δημιουργία του γραμματοκιβωτίου χρήστη στον διακομιστή "
"Dovecot"

#: personal/mail/mail-methods/class_mail-methods-dovecot.inc:185
#, php-format
msgid "Error while trying to contact Dovecot server through Argonaut: %s"
msgstr ""
"Σφάλμα κατά την προσπάθεια επικοινωνίας με τον διακομιστή Dovecot μέσω του "
"Argonaut: %s"
